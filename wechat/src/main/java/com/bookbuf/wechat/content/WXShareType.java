package com.bookbuf.wechat.content;

/**
 * Created by robert on 16/6/30.
 */
public enum WXShareType {
	TEXT ("text");

	String value;

	WXShareType (String value) {
		this.value = value;
	}

	public String getValue () {
		return value;
	}
}
