package com.bookbuf.wechat.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by robert on 16/6/30.
 */
public class WXAuthUtils {
	public WXAuthUtils () {
	}

	public static String request (String urlStr) {
		String emptyStr = "";

		try {
			URL e = new URL (urlStr);
			URLConnection conn = e.openConnection ();
			if (conn == null) {
				return emptyStr;
			} else {
				conn.connect ();
				InputStream inputStream = conn.getInputStream ();
				return inputStream == null ? emptyStr : convertStreamToString (inputStream);
			}
		} catch (Exception var5) {
			var5.printStackTrace ();
			return emptyStr;
		}
	}

	private static String convertStream (InputStream inputStream) throws IOException {
		BufferedReader reader = null;
		reader = new BufferedReader (new InputStreamReader (inputStream));

		String line;
		String result;
		for (result = ""; (line = reader.readLine ()) != null; result = result + line) {
			;
		}

		inputStream.close ();
		return result;
	}

	public static String convertStreamToString (InputStream is) {
		BufferedReader reader = new BufferedReader (new InputStreamReader (is));
		StringBuilder sb = new StringBuilder ();
		String line = null;

		try {
			while ((line = reader.readLine ()) != null) {
				sb.append (line + "/n");
			}
		} catch (IOException var13) {
			var13.printStackTrace ();
		} finally {
			try {
				is.close ();
			} catch (IOException var12) {
				var12.printStackTrace ();
			}

		}

		return sb.toString ();
	}

	public final static class APIParamUtil {

		public static String buildTransaction (String type) {
			return type == null ? String.valueOf (System.currentTimeMillis ()) : type + System.currentTimeMillis ();
		}
	}
}
