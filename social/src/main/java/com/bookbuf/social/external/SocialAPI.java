package com.bookbuf.social.external;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.handlers.ISSOHandler;
import com.bookbuf.social.handlers.IShareHandler;
import com.bookbuf.social.handlers.InternalFacade;
import com.bookbuf.social.handlers.SSOHandler;
import com.bookbuf.social.handlers.share.action.ShareAction;

/**
 * Created by robert on 16/6/29.
 */
public class SocialAPI {

	private static SocialAPI instance = null;
	private InternalFacade serviceImpl;

	private SocialAPI (Context context) {
		serviceImpl = new InternalFacade (context);
		// TODO: 16/6/29 检查社会化组件是否新安装
	}

	public static SocialAPI getInstance (Context context) {
		if (instance == null) {
			instance = new SocialAPI (context);
		}
		return instance;
	}

	public void runOauthApply (Activity activity, PlatformEnum platform, ISSOHandler.AuthListener listener) {
		if (activity != null) {
			// TODO: 16/6/29 起后台线程..显示进度条
			serviceImpl.applyAuthorize (activity, platform, listener);
		}
	}

	public void runOauthDelete (Activity activity, PlatformEnum platform, ISSOHandler.AuthListener listener) {
		if (activity != null) {
			// TODO: 16/6/29 起后台线程..显示进度条
			serviceImpl.deleteAuthorize (activity, platform, listener);
		}
	}

	public void runProfileGet (Activity activity, PlatformEnum platform, ISSOHandler.AuthListener listener) {
		if (activity != null) {
			// TODO: 16/6/29 起后台线程..显示进度条
			serviceImpl.getPlatformInfo (activity, platform, listener);
		}
	}

	public boolean isInstalled (Activity activity, PlatformEnum platform) {
		return serviceImpl.isInstalled (activity, platform);
	}

	public boolean isAuthorized (Activity activity, PlatformEnum platform) {
		return serviceImpl.isAuthorized (activity, platform);
	}

	public void runShare (Activity activity, ShareAction action, IShareHandler.ShareListener listener) {
		if (activity != null) {
			serviceImpl.share (activity, action, listener);
		}
	}

	public void openShareBoard (Activity activity, ShareAction action) {
		if (activity != null) {
			action.openShareBoard ();
		}
	}

	public SSOHandler<?> getHandler (PlatformEnum platform) {
		return serviceImpl != null ? serviceImpl.obtainHandler (platform) : null;
	}

	public void onActivityResult (int requestCode, int resultCode, Intent data) {
		if (serviceImpl != null) {
			serviceImpl.onActivityResult (requestCode, resultCode, data);
		}
	}
}
