package com.bookbuf.social.util;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

/**
 * 线程调度
 */
public class QueuedWork {
	private static Handler uiHandler = new Handler (Looper.getMainLooper ());

	public QueuedWork () {
	}

	public static void runInMain (Runnable runnable) {
		uiHandler.post (runnable);
	}

	public static void runInBack (Runnable runnable) {
		HandlerThread thread = new HandlerThread ("social", 10);
		thread.start ();
		Handler handler = new Handler (thread.getLooper ());
		handler.post (runnable);
	}

}
