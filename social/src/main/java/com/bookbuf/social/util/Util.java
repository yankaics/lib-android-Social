package com.bookbuf.social.util;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by robert on 16/6/30.
 */
public class Util {

	public static Map<String, String> jsonToMap (String json) {
		HashMap map = new HashMap ();

		try {
			JSONObject jsonObject = new JSONObject (json);
			Iterator iterator = jsonObject.keys ();

			while (iterator.hasNext ()) {
				String key = (String) iterator.next ();
				map.put (key, String.valueOf (jsonObject.get (key)));
			}
		} catch (Exception e) {
			e.printStackTrace ();
		}

		return map;
	}

}
