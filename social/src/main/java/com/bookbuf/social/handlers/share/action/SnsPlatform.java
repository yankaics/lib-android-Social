package com.bookbuf.social.handlers.share.action;

import com.bookbuf.social.PlatformEnum;

/**
 * Created by robert on 16/6/29.
 */
public final class SnsPlatform {
	public String mKeyword;
	public String mShowWord;
	public String mIcon;
	public String mGrayIcon;
	public int mIndex;
	public PlatformEnum mPlatform;

	public SnsPlatform (String keyword) {
		this.mKeyword = keyword;
		this.mPlatform = PlatformEnum.convertToEnum (keyword);
	}

	public SnsPlatform () {
	}

}
