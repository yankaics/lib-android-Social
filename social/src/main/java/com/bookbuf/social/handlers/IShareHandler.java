package com.bookbuf.social.handlers;

import android.app.Activity;

import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.handlers.share.content.ShareContent;

/**
 * Created by robert on 16/6/29.
 */
public interface IShareHandler {

	boolean share (Activity activity, ShareContent content, ShareListener listener);


	interface ShareListener {
		void onResult (PlatformEnum shareMedia);

		void onError (PlatformEnum shareMedia, Throwable throwable);

		void onCancel (PlatformEnum shareMedia);
	}
}
