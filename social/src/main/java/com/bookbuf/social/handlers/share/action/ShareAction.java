package com.bookbuf.social.handlers.share.action;

import android.app.Activity;

import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.handlers.IShareHandler;
import com.bookbuf.social.handlers.share.content.ShareContent;
import com.bookbuf.social.handlers.share.content.location.Location;
import com.bookbuf.social.handlers.share.content.media.IMediaObject;

import java.lang.ref.WeakReference;

/**
 * 分享行为,展示分享面板响应分享事件
 */
public class ShareAction {

	private String shareForm;
	private ShareContent shareContent = null;
	private PlatformEnum sharePlatform;
	private IShareHandler.ShareListener shareListener;
	private Activity activity;


	public ShareAction (Activity activity) {
		if (activity != null) {
			this.activity = (Activity) (new WeakReference (activity)).get ();
		}
	}

	public ShareAction setShareContent (ShareContent shareContent) {
		this.shareContent = shareContent;
		return this;
	}

	public ShareAction setShareForm (String form) {
		this.shareForm = form;
		return this;
	}

	public ShareAction setSharePlatform (PlatformEnum platform) {
		this.sharePlatform = platform;
		return this;
	}

	public ShareAction setShareListener (IShareHandler.ShareListener shareListener) {
		this.shareListener = shareListener;
		return this;
	}

	public String getShareForm () {
		return shareForm;
	}

	public ShareContent getShareContent () {
		return shareContent;
	}

	public PlatformEnum getSharePlatform () {
		return sharePlatform;
	}

	public IShareHandler.ShareListener getShareListener () {
		return shareListener;
	}

	public void openShareBoard () {
		// TODO: 16/6/29 打开分享面板
	}

	public void closeShareBoard () {
		// TODO: 16/6/29 关闭分享面板
	}

	public void replaceBoardContent (SnsPlatform... snsPlatforms) {
		// TODO: 16/6/29 设置展示的数据 snsPlatform
	}


	public void add2BoardContent (SnsPlatform platformForView) {
		// TODO: 16/6/29 添加展示数据 snsPlatform
	}

	public void share () {
		// TODO: 16/6/29 执行分享
	}

	public static class Builder {

		private ShareContent content = null;

		public Builder () {
			this.content = new ShareContent ();
		}

		public Builder setText (String text) {
			content.mText = text;
			return this;
		}

		public Builder setTitle (String title) {
			content.mTitle = title;
			return this;
		}

		public Builder setTargetUrl (String targetUrl) {
			content.mTargetUrl = targetUrl;
			return this;
		}

		public Builder setLocation (Location location) {
			content.mLocation = location;
			return this;
		}

		public Builder setExtra (IMediaObject media) {
			content.mExtra = media;
			return this;
		}

		public Builder setMedia (IMediaObject media) {
			content.mMedia = media;
			return this;
		}

		public Builder setFollow (String follow) {
			content.mFollow = follow;
			return this;
		}

		public ShareContent build () {
			return content;
		}
	}
}
