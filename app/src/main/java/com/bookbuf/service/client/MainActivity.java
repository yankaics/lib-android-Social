package com.bookbuf.service.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.external.SocialAPI;
import com.bookbuf.social.handlers.ISSOHandler;
import com.bookbuf.social.handlers.IShareHandler;
import com.bookbuf.social.handlers.share.action.ShareAction;
import com.bookbuf.social.handlers.share.content.ShareContent;
import com.bookbuf.service.R;

import java.util.Map;

public class MainActivity extends Activity {

	String TAG = getClass ().getSimpleName ();
	SocialAPI service;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_main);
		service = SocialAPI.getInstance (this);
	}

	public void onClickAuth (View view) {
		PlatformEnum platform = null;
		switch (view.getId ()) {
			case R.id.app_auth_wechat:
				platform = PlatformEnum.WX_SCENE;
				break;
			default:
				break;
		}
		// 调用授权
		service.runOauthApply (this, platform, new ISSOHandler.AuthListener () {
			@Override
			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
			}

			@Override
			public void onError (PlatformEnum platform, int action, Throwable throwable) {
				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
				Log.d (TAG, "[AuthListener] onError: action = " + action);
				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum platform, int action) {
				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
			}
		});

	}

	public void onClickAuthDel (View view) {
		PlatformEnum platform = null;
		switch (view.getId ()) {
			case R.id.app_auth_wechat:
				platform = PlatformEnum.WX_SCENE;
				break;
			default:
				break;
		}
		service.runOauthDelete (this, platform, new ISSOHandler.AuthListener () {
			@Override
			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
			}

			@Override
			public void onError (PlatformEnum platform, int action, Throwable throwable) {
				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
				Log.d (TAG, "[AuthListener] onError: action = " + action);
				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum platform, int action) {
				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
			}
		});
	}

	public void onClickShare (View view) {
		PlatformEnum platform = null;
		switch (view.getId ()) {
			case R.id.app_share_wechat:
				platform = PlatformEnum.WX_SCENE;
				break;
			case R.id.app_share_wechat_timeline:
				platform = PlatformEnum.WX_SCENE_TIMELINE;
				break;
			case R.id.app_share_wechat_favorite:
				platform = PlatformEnum.WX_SCENE_FAVORITE;
				break;
			default:
				break;
		}
		ShareContent shareContent = new ShareAction.Builder ()
				.setTargetUrl ("[A]www.healthbok.com")
				.setText ("分享内容来自 bookbuf 分享组件.")
				.setTitle ("分享标题来自 bookbuf 分享组件.")
				.build ();

		IShareHandler.ShareListener shareListener = new IShareHandler.ShareListener () {
			@Override
			public void onResult (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onResult: shareMedia = " + shareMedia);
			}

			@Override
			public void onError (PlatformEnum shareMedia, Throwable throwable) {
				Log.d (TAG, "[ShareListener] onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onCancel: shareMedia = " + shareMedia);
			}
		};

		ShareAction action = new ShareAction (this)
				.setShareContent (shareContent)
				.setShareForm ("[B]www.healthbok.com")
				.setSharePlatform (platform)
				.setShareListener (shareListener);

		service.runShare (this, action, action.getShareListener ());
	}

	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data) {
		super.onActivityResult (requestCode, resultCode, data);
		service.onActivityResult (requestCode, resultCode, data);
	}
}
